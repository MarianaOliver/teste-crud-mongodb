package com.crudmongo.response;

import java.util.Date;

public class ErrorMessage {
	
	private Date currenteDate;
	private String message;
	
	public ErrorMessage(Date currenteDate, String message) {
		super();
		this.currenteDate = currenteDate;
		this.message = message;
	}

	public Date getCurrenteDate() {
		return currenteDate;
	}

	public void setCurrenteDate(Date currenteDate) {
		this.currenteDate = currenteDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
