package com.crudmongo.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Tabela {
		
	@Id private String id;
	private String produto;
	private Date dataCadastro;
	private double valorReal;
	private double valorDolar;
	private int quantidade;
	
	public Tabela(){}
	
	public Tabela(String id, String produto, Date dataCadastro, double valorReal, double valorDolar, int quantidade) {
		super();
		this.id = id;
		this.produto = produto;
		this.dataCadastro = dataCadastro;
		this.valorReal = valorReal;
		this.valorDolar = valorDolar;
		this.quantidade = quantidade;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public double getValorReal() {
		return valorReal;
	}

	public void setValorReal(double valorReal) {
		this.valorReal = valorReal;
	}

	public double getValorDolar() {
		return valorDolar;
	}

	public void setValorDolar(double valorDolar) {
		this.valorDolar = valorDolar;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tabela other = (Tabela) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
