package com.crudmongo.service;

import java.util.List;
import com.crudmongo.model.Tabela;

public interface TabelaService {

		public List<Tabela> obterTodos();			
		public Tabela update(Tabela tabela);		
		public Tabela criar (Tabela tabela);
		public void remove(String id);			
}
