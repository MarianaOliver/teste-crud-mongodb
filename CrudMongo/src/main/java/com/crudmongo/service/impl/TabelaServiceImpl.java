package com.crudmongo.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crudmongo.model.Tabela;
import com.crudmongo.repository.TabelaRepository;
import com.crudmongo.service.TabelaService;

@Service
public class TabelaServiceImpl implements TabelaService {
	
	@Autowired
	private TabelaRepository tabelaRepository;

	@Override
	public List<Tabela> obterTodos() {		
		return this.tabelaRepository.findAll();
	}

	@Override 
	public Tabela update(Tabela tabela) {	
		return this.tabelaRepository.save(tabela);
	}
	
	@Override
	public Tabela criar(Tabela tabela){		
		return this.tabelaRepository.save(tabela);
	}

	@Override
	public void remove(String id) {
		 this.tabelaRepository.deleteById(id);
	}

}
