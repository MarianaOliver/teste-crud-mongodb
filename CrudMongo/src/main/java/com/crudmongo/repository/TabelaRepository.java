package com.crudmongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.crudmongo.model.Tabela;
@Repository
public interface TabelaRepository extends MongoRepository<Tabela, String> {

}
