package com.crudmongo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.crudmongo.model.Tabela;
import com.crudmongo.service.TabelaService;

@RestController
@RequestMapping("/tabela")
public class TabelaController {
	
	@Autowired
	private TabelaService tabelaService;
	

		@GetMapping
		public List<Tabela> obterTodos(){
			try {
				return this.tabelaService.obterTodos();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@PatchMapping("/{id}")
		public  Tabela obterPorId(@RequestBody Tabela tabela){		
			return this.tabelaService.criar(tabela);
		}
		
		@PostMapping
		public Tabela criar(@RequestBody Tabela tabela){	
			try {
				return this.tabelaService.criar(tabela);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return tabela;
		}
		
		@DeleteMapping("/{id}")
		public void remove(@PathVariable String id) {
			try {
				this.tabelaService.remove(id);

			} catch (Exception e) {
				e.printStackTrace();
			}			
		}		
}
